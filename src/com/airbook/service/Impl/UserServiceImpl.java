package com.airbook.service.Impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.airbook.bean.Constant;
import com.airbook.bean.Register;
import com.airbook.bean.Response;
import com.airbook.bean.UserLogin;
import com.airbook.entity.User;
import com.airbook.repository.UserRepository;
import com.airbook.service.UserService;
import com.airbook.utils.MD5Util;


@Service
public class UserServiceImpl implements UserService {
	
    @Autowired
	UserRepository useRepository;
    
//	public UserRepository getRepository() {
//		return useRepository;
//	}
	
	@Override
	public User getUser(Integer id) {
		// TODO Auto-generated method stub
		return useRepository.findOne(id);
	}

	/* 用户注册
	 * @see com.airbook.service.UserService#regist(com.airbook.entity.User)
	 */
	@Override
	public User regist(Register register) {
		User user=null;
		if(register!=null) {
			
			user=new User();
			user.setNickname(register.getNickname());
			user.setEmail(register.getEmail());
			user.setUserType(Constant.USER_TYPE_APP); //app用户
			user.setUserStatus(Constant.USER_STATUS_OK); //用户状态
			user.setUserCreateTime(new Date());
			String pwd=MD5Util.string2MD5(register.getPassword());
			user.setPassword(pwd);
			return useRepository.saveAndFlush(user);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.airbook.service.UserService#login(com.airbook.entity.User)
	 */
	@Override
	public User login(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.airbook.service.UserService#checkPassword(com.airbook.bean.UserLogin)
	 */
	@Override
	public User checkPassword(UserLogin userLogin) {
		User user=null;
		String loginName=userLogin.getLoginName();
		//判断当前用户是否存在
		List<User> userList = useRepository.loginCheck(loginName,loginName,loginName);
		if (userList!=null&&userList.size()>0) {
			user=userList.get(0);
			String dbPassword = user.getPassword();
			//MD5验证密码是否一致
			if (userLogin.getPassword() != null && dbPassword != null) {
				if (!MD5Util.string2MD5(userLogin.getPassword()).equals(
						dbPassword)) {
					user = null;
				}
			}
		}
		return user;
	}
	
	

}
