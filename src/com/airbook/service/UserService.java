package com.airbook.service;

import com.airbook.bean.Register;
import com.airbook.bean.UserLogin;
import com.airbook.entity.User;

public interface UserService {
	/**
	 * 获取用户信息
	 * @param i
	 * @return
	 */
    public User getUser(Integer i);
    /**
     * 用户注册
     * @param user
     * @return
     */
    public User regist(Register register);
    /**
     * 用户登录
     * @param user
     * @return
     */
    public User login(User user);
    /**
     * 验证输入密码
     * @param userLogin
     * @return
     */
    public User checkPassword(UserLogin userLogin);
}
