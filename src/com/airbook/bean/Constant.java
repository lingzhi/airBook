package com.airbook.bean;

/**
*desc:常亮
*name:Constant.java
*time:2016年11月15日   
*user:created by yanlz.
*/
public class Constant {
	/**返回成功**/
    public static final String RESULT_SUCCESS = "SUCCESS";

    /**D返回失败**/
    public static final String RESULT_FAILED = "FAILED";
    
    /** 用户类型：app */
    public static final int USER_TYPE_APP = 1;

    /** 用户类型：admin */
    public static final int USER_TYPE_ADMIN = 0;
    
    /**
     * 登录用户的额key
     */
    public static final String SESSION_USER_KEY = "user";
    /*
     *  用户状态，0为拉黑状态
     */
    public static final int USER_STATUS_NO = 0;
    /*
     *  用户状态，1为拉黑状态
     */
    public static final int USER_STATUS_OK = 1;
}
