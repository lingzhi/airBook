package com.airbook.bean;

/**
*desc:注册信息
*name:Regedit.java
*time:2016年11月15日   
*user:created by yanlz.
*/
public class Register {
	private String nickname;
	private String email;
    private String mobile;
    private String password;
    private String repassword;
    private String agreeProtocol; //注册协议
    private String phoneVerCode; //手机验证码
    
    
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the repassword
	 */
	public String getRepassword() {
		return repassword;
	}
	/**
	 * @return the agreeProtocol
	 */
	public String getAgreeProtocol() {
		return agreeProtocol;
	}
	/**
	 * @return the phoneVerCode
	 */
	public String getPhoneVerCode() {
		return phoneVerCode;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param repassword the repassword to set
	 */
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	/**
	 * @param agreeProtocol the agreeProtocol to set
	 */
	public void setAgreeProtocol(String agreeProtocol) {
		this.agreeProtocol = agreeProtocol;
	}
	/**
	 * @param phoneVerCode the phoneVerCode to set
	 */
	public void setPhoneVerCode(String phoneVerCode) {
		this.phoneVerCode = phoneVerCode;
	}
    
    
}
