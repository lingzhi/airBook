package com.airbook.bean;

/**
*desc:
*name:UserLogin.java
*time:2016年11月15日   
*user:created by yanlz.
*/
public class UserLogin {
	private String loginName;

    private String password;

    private Integer rememberFlg;

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the rememberFlg
	 */
	public Integer getRememberFlg() {
		return rememberFlg;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param rememberFlg the rememberFlg to set
	 */
	public void setRememberFlg(Integer rememberFlg) {
		this.rememberFlg = rememberFlg;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserLogin [loginName=").append(loginName).append(", password=").append(password)
				.append(", rememberFlg=").append(rememberFlg).append("]");
		return builder.toString();
	}
    
}
