package com.airbook.bean;

/**
*desc:用来同意封装返回数据
*name:Response.java
*time:2016年11月15日   
*user:created by yanlz.
*/
public class Response {
	private String result;
	private String message;
	private Object data;
	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Response [result=").append(result).append(", message=").append(message).append(", data=")
				.append(data).append("]");
		return builder.toString();
	}
	
}
