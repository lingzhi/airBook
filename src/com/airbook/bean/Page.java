package com.airbook.bean;

import java.util.List;

/**
*desc:分页
*name:Page.java
*time:2016年8月7日   
*user:created by yanlz.
*/
public class Page<T> {//范型的使用
    private int page;//当前页
    private int total;//一共多少页
    private long records;//查处结果总行数
    private List<T> rows;//当前页每一行的内容

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

}
