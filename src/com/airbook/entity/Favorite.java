package com.airbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
*desc:收藏
*name:Favorite.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_favorite",schema="airbook")
@Entity
public class Favorite implements Serializable{

	private static final long serialVersionUID = 359114654029238349L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id; //虚拟ID
	
	@Column(name="user_id")
	private Long userId;
//	private User user; //自身
	
	@Column(name="book_id")
	private Long bookId;
//	private Book favoriteBook; //收藏的书籍

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @return the bookId
	 */
	public Long getBookId() {
		return bookId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	
	
	
}
