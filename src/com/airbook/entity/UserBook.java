package com.airbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import javax.persistence.Table;

/**
*desc:用户书籍
*name:UserBook.java
*time:2016年11月7日   
*user:created by yanlz.
*/
@Table(name="a_user_book",schema="airbook")
@Entity
public class UserBook implements Serializable{
	
	private static final long serialVersionUID = -8259082532511482859L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	//对应用户账号
	@Column(name = "user_id")
	private Integer userId;
	
	//对应书籍Id
	@Column(name = "book_id")
	private Integer books;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @return the books
	 */
	public Integer getBooks() {
		return books;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @param books the books to set
	 */
	public void setBooks(Integer books) {
		this.books = books;
	}

	

	
	
	
}
