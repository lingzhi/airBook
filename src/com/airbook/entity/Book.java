package com.airbook.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
* desc:书籍
* name:Book.java
* time:2016年8月7日   
* user:created by yanlz.
 */
@Table(name="a_book",schema="airbook")
@Entity
public class Book implements Serializable{

	private static final long serialVersionUID = 2264734351537004555L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; //书籍ID
	
	@Column(name="book_name")
	private String bookName; //书名
	
	private String author;  //作者
	private String publisher; //出版社
	private Double price; //书价，用户自定义
	private Double deposit; //押金
	
	@Column(name="book_isbn")
	private String bookIsbn; //书籍ISBN
	
	private String picture; //书籍照片地址
	private String description; //书籍描述
	
	@Column(name="rb_status")
	private Integer rbStatus; //书籍状态，0:未出售，1：已租出  rb表示:rent,brrow
	
	@Column(name="a_status")
	private Integer aStatus; //续租状态，a表示again
	
	private String rules; //书籍租阅条例
	
	@Column(name="publish_time")
	private Date publishTime; //书籍上架时间
	private Integer property; //状态判断书籍的所有权
	private Integer recommend; //推荐 （0：不推荐，1：推荐）
	
	@Column(name="category_id")
	private Integer categoryId;
//	private Category category; //书籍类别
	
	@Column(name="user_Id")
	private Integer userId;
//	private User user; //书的主人
	
	@Column(name="b_user_id")
	private Integer bUserId;
//	private User bUser; //借书人 （b:borrow）

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the bookName
	 */
	public String getBookName() {
		return bookName;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @return the deposit
	 */
	public Double getDeposit() {
		return deposit;
	}

	/**
	 * @return the bookIsbn
	 */
	public String getBookIsbn() {
		return bookIsbn;
	}

	/**
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the rbStatus
	 */
	public Integer getRbStatus() {
		return rbStatus;
	}

	/**
	 * @return the aStatus
	 */
	public Integer getaStatus() {
		return aStatus;
	}

	/**
	 * @return the rules
	 */
	public String getRules() {
		return rules;
	}

	/**
	 * @return the publishTime
	 */
	public Date getPublishTime() {
		return publishTime;
	}

	/**
	 * @return the property
	 */
	public Integer getProperty() {
		return property;
	}

	/**
	 * @return the recommend
	 */
	public Integer getRecommend() {
		return recommend;
	}

	/**
	 * @return the categoryId
	 */
	public Integer getCategoryId() {
		return categoryId;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @return the bUserId
	 */
	public Integer getbUserId() {
		return bUserId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param bookName the bookName to set
	 */
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	/**
	 * @param bookIsbn the bookIsbn to set
	 */
	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}

	/**
	 * @param picture the picture to set
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param rbStatus the rbStatus to set
	 */
	public void setRbStatus(Integer rbStatus) {
		this.rbStatus = rbStatus;
	}

	/**
	 * @param aStatus the aStatus to set
	 */
	public void setaStatus(Integer aStatus) {
		this.aStatus = aStatus;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(String rules) {
		this.rules = rules;
	}

	/**
	 * @param publishTime the publishTime to set
	 */
	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	/**
	 * @param property the property to set
	 */
	public void setProperty(Integer property) {
		this.property = property;
	}

	/**
	 * @param recommend the recommend to set
	 */
	public void setRecommend(Integer recommend) {
		this.recommend = recommend;
	}

	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @param bUserId the bUserId to set
	 */
	public void setbUserId(Integer bUserId) {
		this.bUserId = bUserId;
	}

	
	
	
		
}
