package com.airbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
*desc:评价类
*name:Evaluation.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_evaluation",schema="airbook")
@Entity
public class Evaluation implements Serializable{

	private static final long serialVersionUID = 359114654029238349L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id; //ID
	
	@Column(name="user_id")
	private Long userId;   
//	private User user; //评价者
	
	@Column(name="book_id")
	private Long bookId;
//	private Book book; //评价的书籍
	
	private String content; //评价内容
	private Integer start; //评价星级 
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @return the bookId
	 */
	public Long getBookId() {
		return bookId;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @return the start
	 */
	public Integer getStart() {
		return start;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(Integer start) {
		this.start = start;
	}
	
	
}
