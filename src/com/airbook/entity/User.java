package com.airbook.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * 
*desc:用户表
*name:User.java
*time:2016年7月7日   
*user:created by yanlz.
 */

@Table(name="a_user",schema="airbook")
@Entity
public class User implements Serializable{
	private static final long serialVersionUID = 3576558826437032294L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//采用数据库 ID自增长的方式来自增主键字段,auto为jpa自动选择合适的策略，是默认选项
	private Integer id; //用户ID
	
	private String nickname; //用户昵称
	
	@Column(name="password")
	private String password; //用户密码
	
	@Column(name="true_name")
	private String trueName; //用户真实姓名
	

	private String gender; //性别
	
	@Column(name="birth_date")
	private Date birthDate; //出生日期
	
	@Column(name="phone_number")
	private String phoneNumber;  //电话号码
	
	@Column(name="email")
	private String email; //邮箱
	private String address; //地址
	private String introduce; //介绍
	private String photo; // 头像路径
	
	@Column(name="user_status")
	private Integer userStatus; //'0':拉黑，'1'：正常
	
	@Column(name="user_type")
	private Integer userType;
	
	@Column(name="inline_status")
	private Integer inlineStatus; //在线状态（0：线下，1:在线）
	
	@Column(name="user_create_time")
	private Date userCreateTime;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the trueName
	 */
	public String getTrueName() {
		return trueName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the introduce
	 */
	public String getIntroduce() {
		return introduce;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @return the inlineStatus
	 */
	public Integer getInlineStatus() {
		return inlineStatus;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param trueName the trueName to set
	 */
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @param introduce the introduce to set
	 */
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @param inlineStatus the inlineStatus to set
	 */
	public void setInlineStatus(Integer inlineStatus) {
		this.inlineStatus = inlineStatus;
	}

	/**
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * @return the userStatus
	 */
	public Integer getUserStatus() {
		return userStatus;
	}

	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * @return the userCreateTime
	 */
	public Date getUserCreateTime() {
		return userCreateTime;
	}

	/**
	 * @param userCreateTime the userCreateTime to set
	 */
	public void setUserCreateTime(Date userCreateTime) {
		this.userCreateTime = userCreateTime;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=").append(id).append(", nickname=").append(nickname).append(", password=")
				.append(password).append(", trueName=").append(trueName).append(", gender=").append(gender)
				.append(", birthDate=").append(birthDate).append(", phoneNumber=").append(phoneNumber)
				.append(", email=").append(email).append(", address=").append(address).append(", introduce=")
				.append(introduce).append(", photo=").append(photo).append(", userStatus=").append(userStatus)
				.append(", userType=").append(userType).append(", inlineStatus=").append(inlineStatus)
				.append(", userCreateTime=").append(userCreateTime).append("]");
		return builder.toString();
	}

	
	
	

	
	
	

//	private List<Book> myBooks=new ArrayList<Book>(); //上架书籍,用户发布的所有书籍
//	private List<Book> inBooks=new ArrayList<Book>(); //借入书籍
//	private List<Book> outBook=new ArrayList<Book>(); //借出书籍
//	private List<Book> favoriteBook=new ArrayList<Book>(); //收藏的书籍
//	private List<User> focusUsers=new ArrayList<User>(); //关注的用户 
	
	
	
	
	
	
	

}
