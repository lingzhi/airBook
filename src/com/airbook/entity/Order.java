package com.airbook.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
*desc:订单
*name:Order.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_order",schema="airbook")
@Entity
public class Order implements Serializable {

	private static final long serialVersionUID = -157475863653468763L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId; //购买用户
	
	@Column(name="book_id")
	private Integer bookId; 
//	private Book book;    //租阅书籍信息
	
	@Column(name="order_id")
	private String orderId; //订单编号
	
	@Column(name="order_time")
	private Date orderTime; //订单时间
	
	@Column(name="book_count")
	private Integer bookCount; //书籍数量
	
	@Column(name="total_price")
	private Double totalPrice; //订单总价
	
	@Column(name="hier_day")
	private Integer hireDay; //租期
	
	@Column(name="end_time")
	private Date endTime;  //租期截止时间

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @return the bookId
	 */
	public Integer getBookId() {
		return bookId;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @return the orderTime
	 */
	public Date getOrderTime() {
		return orderTime;
	}

	/**
	 * @return the bookCount
	 */
	public Integer getBookCount() {
		return bookCount;
	}

	/**
	 * @return the totalPrice
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @return the hireDay
	 */
	public Integer getHireDay() {
		return hireDay;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @param orderTime the orderTime to set
	 */
	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	/**
	 * @param bookCount the bookCount to set
	 */
	public void setBookCount(Integer bookCount) {
		this.bookCount = bookCount;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @param hireDay the hireDay to set
	 */
	public void setHireDay(Integer hireDay) {
		this.hireDay = hireDay;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	
	
	

}
