package com.airbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
*desc:购物车
*name:Cart.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_cart",schema="airbook")
@Entity
public class Cart implements Serializable{

	private static final long serialVersionUID = 359114654029238349L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id; //购物车
	
	private Integer quantity;// 商品数量
	
	@Column(name="total_price")
	private Double totalPrice; //商品总价
	
	@Column(name="user_id")
	private Integer userId; //当前用户
	
	@Column(name="book_id")
	private Integer bookId;
//	private Book book; // 放入购物车的商品

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @return the totalPrice
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @return the bookId
	 */
	public Integer getBookId() {
		return bookId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @param bookId the bookId to set
	 */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	
	
	
}
