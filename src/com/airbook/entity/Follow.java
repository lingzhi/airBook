package com.airbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
*desc:关注
*name:Follow.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_follow",schema="airbook")
@Entity
public class Follow implements Serializable{

	private static final long serialVersionUID = 359114654029238349L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id; //虚拟ID
	
	@Column(name="user_id")
	private Integer userId;
//	private User user; //自身
	
	@Column(name="f_user_id")
	private Integer fUserId;
//	private User fUser; //关注的人

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @return the fUserId
	 */
	public Integer getfUserId() {
		return fUserId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @param fUserId the fUserId to set
	 */
	public void setfUserId(Integer fUserId) {
		this.fUserId = fUserId;
	}
	
	
	
	
	
}
