package com.airbook.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*desc:书籍种类
*name:Category.java
*time:2016年8月7日   
*user:created by yanlz.
 */
@Table(name="a_category",schema="airbook")
@Entity
public class Category implements Serializable {
	
	
	private static final long serialVersionUID = -5889838884062962297L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private  Integer id;//种类ID
	
	@Column(name="category_name")
	private String categoryName; //类名

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
//	private List<Book> books=new ArrayList<Book>(); //种类下包含多少书
	
	
	
}
