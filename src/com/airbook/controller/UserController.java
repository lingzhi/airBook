package com.airbook.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.airbook.bean.Constant;
import com.airbook.bean.Register;
import com.airbook.bean.Response;
import com.airbook.bean.UserLogin;
import com.airbook.entity.User;
import com.airbook.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	private int cookieExpireTime=259200; // 读取全局设置的cookie过期时间

	@Autowired
	private UserService userSerivce;

	@RequestMapping(value = "/welcome")
	public ModelAndView getUser(ModelAndView modelView) {
		User user = userSerivce.getUser(1);
		return new ModelAndView("welcome", "user", user);
	}

//	/**
//	 * 注册分发
//	 * 
//	 * @param register
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping(value = "/regist", method = RequestMethod.POST)
//	@ResponseBody
//	public Response regedit(Register register, HttpServletRequest request) {
//		String t = request.getParameter("t");
//		if (null != t && "m".equals(t)) {
//			return mobileRegedit(register, request);
//		} else {
//			return emailRegedit(register, request);
//		}
//	}

//	/**
//	 * 邮箱注册
//	 * 
//	 * @param register
//	 * @param request
//	 * @return
//	 */
//	private Response emailRegedit(Register register, HttpServletRequest request) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	/**
//	 * 手机号注册
//	 * 
//	 * @param register
//	 * @param request
//	 * @return
//	 */
//	private Response mobileRegedit(Register register, HttpServletRequest request) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	/**
	 * 用户注册
	 * @param register
	 * @return
	 */
	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ResponseBody
	public Response regist(Register register) {
		Response response=new Response();
		userSerivce.regist(register);
		response.setResult(Constant.RESULT_SUCCESS);
		response.setMessage(Constant.RESULT_SUCCESS);
		response.setData("/airbook/jump.jsp");
		return response;
	}

	/**
	 * 登录调度
	 * 
	 * @param userLogin
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public Response login(UserLogin userLogin, HttpServletRequest request, HttpServletResponse response) {
		String method = request.getParameter("method");
		if (null != method && "phone".equals(method)) {
			return mobileLogin(userLogin, request, response);
		} else {
			return emailLogin(userLogin, request, response);
		}
		// return emailLogin(userLogin,request,response);
	}

	/**
	 * 邮件/用户名登录
	 * @param userLogin
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/emailLogin", method = RequestMethod.POST)
	@ResponseBody
	private Response emailLogin(UserLogin userLogin, HttpServletRequest request, HttpServletResponse response) {
		Response resp = new Response();
		User user = userSerivce.checkPassword(userLogin);
		if (user != null) {
			// 设置登录用户缓存
			resp = setUserIntoSession(request, response, userLogin, user);
			resp.setMessage("登录登录成功");
			resp.setResult(Constant.RESULT_SUCCESS);
			resp.setData("/airbook/index.jsp");
			return resp;
		} else {
			resp.setMessage("登陆账号或密码不正确");
			resp.setResult(Constant.RESULT_FAILED);
			resp.setData("/airbook/index.jsp");
			return resp;
		}
		
	}

	/**
	 * 设置用户信息入session
	 * 
	 * @param request
	 * @param response
	 * @param userLogin
	 * @param user
	 * @return
	 */
	private Response setUserIntoSession(HttpServletRequest request, HttpServletResponse response, UserLogin userLogin,
			User user) {
		Response resp = new Response();
		HttpSession session = request.getSession(true);
		String result = "/index.jsp";

		session.setAttribute(Constant.SESSION_USER_KEY, user);
		// 如果选择了自动登录，保存cookie
		if (userLogin.getRememberFlg() != null && "1".equals(userLogin.getRememberFlg())) {
			Cookie cookie = new Cookie("userId", user.getId().toString());
			cookie.setMaxAge(cookieExpireTime);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
		resp.setResult(Constant.RESULT_SUCCESS);
		resp.setMessage(Constant.RESULT_SUCCESS);
		resp.setResult(result);
		return resp;
	}

	/**
	 * 手机号登录
	 * @param userLogin
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/mobileLogin", method = RequestMethod.POST)
	@ResponseBody
	private Response mobileLogin(UserLogin userLogin, HttpServletRequest request, HttpServletResponse response) {
		return null;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public boolean logout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(true);

		User user = (User) session.getAttribute("user");
		if (user != null) {
			session.removeAttribute("user");
			session.invalidate();

			// 删除cookie
			Cookie cookie = new Cookie("userId", user.getId().toString());
			cookie.setMaxAge(0);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
		return true;
	}

}
