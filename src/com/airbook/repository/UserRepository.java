package com.airbook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.airbook.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	@Modifying
	@Query("from  #{#entityName} o where o.userStatus=1 and (o.nickname=:nickname or o.email=:email or o.phoneNumber=:phoneNumber) ")
    public List<User> loginCheck(@Param("nickname") String nickname,@Param("email") String email,@Param("phoneNumber") String phoneNumber);
}
