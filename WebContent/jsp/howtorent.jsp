<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>在airbook上出租你的图书</title>
		<!--ç½ç«å¾æ -->
		<link href="../img/airbook16.ico" rel="shortcut icon">
		<link href="../css/bootstrap.css" rel="stylesheet">
		<!--<link href="../css/index.css" rel="stylesheet" type="text/css" />-->
		<link href="../css/howtorent.css" rel="stylesheet" type="text/css" />
		<script src="../js/html5shiv.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</head>

	<body>
		<!--å¯¼èªæ¡-->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header col-lg-1">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../index.jsp" aria-label="Airbnb">
						<img alt="Brand" src="../img/logoko3.png">
					</a>
				</div>
				<div class="col-lg-7">
					<form class="navbar-form navbar-left" role="search">
						<!--<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" required>
							<span class="input-group-btn">
        					<button class="btn btn-default" type="button">Go!</button>
      						</span>
			
						</div>-->
						<div class="input-group">
							<input type="text" class="form-control">
							<span class="input-group-btn">
        						<button class="btn btn-default glyphicon glyphicon-search" type="button" />
      						</span>
						</div>
						<!--<button type="submit" class="btn btn-default">æäº¤</button>-->
					</form>
				</div>

				<div class="col-lg-4">
					<ul class="nav navbar-nav navbar-right">
						<li class="plan"><a href="#"> 计划</a></li>
						<li class="info"><a href="#"> 消息</a></li>
						<li class="usrimg2">
							<a href="#"><img src="../img/pic1.jpg" class="img-circle"></a>
						</li>
					</ul>

				</div>
			</div>
		</nav>
		<!--
        	描述：上传书籍
        -->
		<div class="container-fluid upload">
			<div class="row-fluid">
				<div class="col-lg-5 bookinfo">
					<p>凌志，您好！我们来为您做好当房东的准备吧.</p>
					<h4>您拥有什么样的图书？</h4>
					<div class="form">
						<form class="book">
							<select>
								<option value="人文">文学种类</option>
								<option value="历史">历史</option>
								<option value="ecloginm">经济</option>
							</select>
							<input type="text" name="bookName" class="bookName" placeholder="书籍名称" />
							<br />
							<input type="text" name="ISBN" class="isbn" placeholder="书籍ISBN" />
							<input type="text" name="author" class="author" placeholder="作者">
							<br />
							<input type="text" name="price" class="price" placeholder="价格">
							<input type="file" name="picture" class="picture" />
							<input type="button" name="button" class="submit" value="提交" />
						</form>
					</div>
				</div>
				<div class="col-lg-7 bookpicture">
					<img alt="background-book" src="../img/book3.jpg" /> </div>
			</div>
		</div>
		<!--
            	footer
            -->
			<div class="container-fluid footer">
				<div class="row-fluid">
					<div class="col-lg-3 input">
						<select name="English">
							<option value="english">English(英文)</option>
						</select>
					</div>
					<div class="col-lg-3 airbook">
						<h4>Airbook</h4>
						<p>关于
							<br>工作机会
							<br>新闻
							<br>政策
							<br>帮助</p>
					</div>
					<div class="col-lg-3 find">
						<h4>发现</h4>
						<p>信任与安全
							<br>读书基金
							<br>礼品卡
							<br>Airbook行动力
							<br>读书指南</p>
					</div>
					<div class="col-lg-3 rent">
						<h4>出租</h4>
						<p>为什么要出租?
							<br>好读之道
							<br>书主义务
							<br>
						</p>
					</div>
				</div>
			</div>
			<div class="container footermine">
				<footer>
					<p class="pull-right"><a href="#top">go back</a></p>
					<p>@2016 airbook</p>
				</footer>
			</div>

		<script src="../js/jquery-1.11.1.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>

</html>