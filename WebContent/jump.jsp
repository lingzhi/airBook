<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<title>注册成功</title>
	<meta charset="utf-8">
    <link rel="icon" href="/img/airbook.png" type="image/x-icon" />
    <link rel="shortcut icon" href="/img/airbook.png" type="image/x-icon" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="5;URL='/airbook/index.jsp'">
</head>
  
  <body>
    <div class="header">
        <div class="headercer">
            <div class="logo">
                <img src="img/airbook.png" width="120" />
            </div>
            <a href="login.jsp">登录</a>
        </div>
    </div>
    <ul class="Resetul">
        <li class="biaoti">
         	您已注册成功，<span id="countdown">3</span>秒后跳转至登录页。<br>如果无法自动跳转，请<a href="/login.jsp" style="color:blue;">点击此处</a>
        </li>
    </ul>
  </body>
<script type="text/javascript">
	var int = self.setInterval("clock()", 1000);
	function clock() {
		var sec = document.getElementById("countdown").innerHTML;
		var t = parseInt(sec);
		if (t > 0) {
			t = t - 1;
			document.getElementById("countdown").innerHTML = t;
		} else {
			window.clearInterval(int);
		}
	}
</script>
</html>
