<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>airBook</title>
<!--网站图标-->
<link href="img/airbook16.ico" rel="shortcut icon">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/index.css" rel="stylesheet" type="text/css" />
<script src="js/html5shiv.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>
	<!--导航条-->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<c:choose>
			<c:when test="${not empty user }">
				<!--描述：登录后的导航条-->
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header col-lg-1">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#" aria-label="Airbnb"> <img
							alt="Brand" src="img/logoko3.png">
						</a>
					</div>
					<div class="col-lg-7">
						<form class="navbar-form navbar-left" role="search">
							<!--<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" required>
							<span class="input-group-btn">
        					<button class="btn btn-default" type="button">Go!</button>
      						</span>
			
						</div>-->
							<div class="input-group">
								<input type="text" class="form-control"> <span
									class="input-group-btn">
									<button class="btn btn-default glyphicon glyphicon-search"
										type="button" />
								</span>
							</div>
							<!--<button type="submit" class="btn btn-default">提交</button>-->
						</form>
					</div>

					<div class="col-lg-4">
						<ul class="nav navbar-nav navbar-right">
							<li class="user2"><a href="jsp/howtorent.jsp">成为书主</a></li>
							<li class="plan"><a href="#"> 计划</a></li>
							<li class="info"><a href="#"> 消息</a></li>
							<li class="usrimg2"><a href="#"><img src="img/pic1.jpg"
									class="img-circle"></a></li>
						</ul>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header col-lg-1">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#" aria-label="Airbnb"> <img
							alt="Brand" src="img/logoko3.png">
						</a>
					</div>
					<div class="col-lg-7">
						<form class="navbar-form navbar-left" role="search">
							<!--<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" required>
							<span class="input-group-btn">
        					<button class="btn btn-default" type="button">Go!</button>
      						</span>
			
						</div>-->
							<div class="input-group">
								<input type="text" class="form-control"> <span
									class="input-group-btn">
									<button class="btn btn-default glyphicon glyphicon-search"
										type="button" />
								</span>
							</div>
							<!--<button type="submit" class="btn btn-default">提交</button>-->
						</form>
					</div>

					<div class="col-lg-4">
						<ul class="nav navbar-nav navbar-right">
							<li class="user"><a href="#"><span
									class="glyphicon glyphicon-user"></span> 成为书主</a></li>
							<li class="regist" data-toggle="modal"
								data-target="#signin-signup-tab" id="signup-button"><a
								href="#"><span class="glyphicon glyphicon-registration-mark"></span>
									注册</a></li>
							<li class="login" data-toggle="modal"
								data-target="#signin-signup-tab" id="signin-button"><a
								href="#"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
							<!--<li class="img"><a href="#"><span ></span> 成为书主</a></li>-->
						</ul>

					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</nav>
	<div class="container">

		<div id="saying">
			<p>
				<span>读万卷书，行万里路。</span>外物之味，久则可厌， <br>读书之味，愈久愈深。
			</p>
		</div>
		<div id="serach">
			<div class="row">

				<div class="col-lg-5">
					<label> <span>图书</span>
					</label> <input type="text" class="form-control" placeholder="书名">
				</div>
				<div class="col-lg-5">
					<label> <span>时间</span>
					</label> <input type="text" class="form-control" placeholder="租阅时间">
				</div>
				<div class="col-lg-2">
					<button class=" glyphicon glyphicon-search" type="button">
						搜索</button>
				</div>
			</div>
		</div>
		<div>
			<p class="selectTitle">
				推荐Airbook精选 <a class="moreright" href="#">查看更多</a>
			</p>
		</div>
		<div class="row select">

			<div class="col-md-4">
				<a href="#"><img class="img-responsive" src="img/pic2.jpg" /></a>
				<p>
					<a href="#"><span>￥385 </span>费城的独立房间<br>415条评价</a>
				</p>
			</div>
			<div class="col-md-4">
				<a href=""><img class="img-responsive" src="img/pic1.jpg" /></a>
				<p>
					<a href="#"><span>￥385 </span> 费城的独立房间<br>415条评价</a>
				</p>
			</div>
			<div class="col-md-4">
				<a href=""><img class="img-responsive" src="img/pic3.jpg" /></a>
				<p>
					<a href="#"><span>￥385 </span>费城的独立房间<br>415条评价</a>
				</p>
			</div>
		</div>
		<hr class="divider" />

		<div class="bookname">
			<p>探索书海</p>
		</div>
		<div id="book">

			<div class="bookpicture">
				<div id="pic1" class="row">
					<div class="col-lg-6 left">
						<div class="booktitle">ddddd</div>
						<div class="style">
							<a href=""><img alt="Brand" src="img/pic1.jpg"></a>
						</div>
					</div>
					<div class="col-lg-6 right">
						<div class="toppic">
							<div class="ltpic">
								<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
							</div>
							<div class="rtpic">
								<a href=""><img alt="Brand" src="img/pic3.jpg"></a>
							</div>
						</div>
						<div class="bottom">
							<div class="lbpic">
								<a href=""><img alt="Brand" src="img/pic4.jpg"></a>
							</div>
							<div class="rbpic">
								<a href=""><img alt="Brand" src="img/pic5.jpg"></a>
							</div>
						</div>
					</div>
				</div>
				<div id="pic2" class="row">
					<div class="col-lg-9 leftpic">
						<div class="slpic">
							<div class="sltpic">
								<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
							</div>
							<div class="slbpic">
								<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
							</div>
						</div>
						<div class="centerpic">
							<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
						</div>
					</div>
					<div class="col-lg-3 rightpic">

						<div class="srtpic">
							<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
						</div>
						<div class="srbpic">
							<a href=""><img alt="Brand" src="img/pic2.jpg"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr class="divider" />
		<div class="readingstrategy">
			<p>读书攻略</p>
		</div>
		<!--<div class="row select strategy">
				
				<div class="col-md-4">
					<a href="#"><img class="img-circle" src="WEB-INF/img/pic2.jpg" /></a>
					<p><a href="#"><span>￥385 </span>费城的独立房间<br>415条评价</a></p>
				</div>
				<div class="col-md-4">
					<a href=""><img class="img-thumbnail" src="WEB-INF/img/pic1.jpg" /></a>
					<p ><a href="#"><span>￥385 </span> 费城的独立房间<br>415条评价</a></p>				
				</div>
				<div class="col-md-4">
					<a href=""><img class="img-circle" src="WEB-INF/img/pic3.jpg" /></a>
					<p ><a href="#"><span>￥385 </span>费城的独立房间<br>415条评价</a></p>			
				</div>	
			</div>-->
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist" id="tablist">
			<li role="presentation" class="active"><a href="#china"
				role="tab" data-toggle="tab">人文</a></li>
			<li role="presentation"><a href="#usa" role="" data-toggle="tab">经济</a></li>
			<li role="presentation"><a href="#india" role="tab"
				data-toggle="tab">历史</a></li>
			<li role="presentation"><a href="#egpty" role="tab"
				data-toggle="tab">政治</a></li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="china">
				<div class="row feature">
					<div class="col-sm-5">
						<img class="feature-image img-responsive" src="img/pic6.jpg" />
					</div>
					<div class="col-sm-7">
						<h2 class="feature-heading">
							经济<span class="text-muted">古老的</span>
						</h2>
						<p class="lead">中国是以华夏文明为源泉、中华文化为基础并以汉族为主体民族的多民族国家，通用汉语。</p>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="usa">
				<div class="row feature">
					<div class="col-sm-5">
						<h2 class="feature-heading">
							历史<span class="text-muted">兴起的</span>
						</h2>
						<p class="lead">美国，是由华盛顿哥伦比亚特区、50个州和关岛等众多海外领土组成的联邦共和立宪制国家.</p>
					</div>
					<div class="col-sm-7">
						<img class="feature-image img-responsive" src="img/pic5.jpg" />
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="india">
				<div class="row feature">
					<div class="col-sm-5">
						<img class="feature-image img-responsive" src="img/pic4.jpg" />
					</div>
					<div class="col-sm-7">
						<h2 class="feature-heading">
							政治<span class="text-muted">悠久的</span>
						</h2>
						<p class="lead">印度，位于10°N-30°N之间，南亚次大陆最大国家。</p>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="egpty">
				<div class="row feature">
					<div class="col-sm-7">
						<h2 class="feature-heading">
							Egpty<span class="text-muted">长久的 </span>
						</h2>
						<p class="lead">埃及，位于北非东部，领土还包括苏伊士运河以东、亚...</p>
					</div>
					<div class="col-sm-5">
						<img class="feature-image img-responsive" src="img/pic2.jpg" />
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid contect">
		<div class="row-fluid">
			<div class="col-lg-4">
				<div class="style">
					<div class="icon">
						<img src="img/phone.JPG" />
					</div>
					<div class="text">
						<strong>173-2697-5272</strong>
						<p>
							24小时客服电话号码。我们在 <br>8:00 - 22:00提供服务。
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="style">
					<div class="icon1">
						<img src="img/alipay.JPG" />
					</div>
					<div class="text">
						<strong>安全支付</strong>
						<p>
							支付宝付款已向用户开通， <br>欢迎使用。
						</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="style">
					<div class="icon">
						<img src="img/wechat.JPG" />
					</div>
					<div class="text">
						<strong>线下联系</strong>
						<p>房东和房客可以通过实地交易。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid footer">
		<div class="row-fluid">
			<div class="col-lg-3 input">
				<select name="English">
					<option value="english">English(英文)</option>
				</select>
			</div>
			<div class="col-lg-3 airbook">
				<h4>Airbook</h4>
				<p>
					关于 <br>工作机会 <br>新闻 <br>政策 <br>帮助
				</p>
			</div>
			<div class="col-lg-3 find">
				<h4>发现</h4>
				<p>
					信任与安全 <br>读书基金 <br>礼品卡 <br>Airbook行动力 <br>读书指南
				</p>
			</div>
			<div class="col-lg-3 rent">
				<h4>出租</h4>
				<p>
					为什么要出租? <br>好读之道 <br>书主义务 <br>
				</p>
			</div>
		</div>
	</div>
	<div class="container footermine">
		<footer>
			<p class="pull-right">
				<a href="#top">回到顶部</a>
			</p>
			<p>@2016空客A380</p>
		</footer>
	</div>
	<!--旋转木马-->
	<!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			/**
			 * Indicators
			 */
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>

			/**
			 * Wrapper for slides
			 */
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="WEB-INF/img/carousel1.jpg" alt="carousel1">
					<div class="carousel-caption">
						...
					</div>
				</div>
				<div class="item">
					<img src="WEB-INF/img/carousel2.jpg" alt="carousel2">
					<div class="carousel-caption">
						...
					</div>
				</div>
				<div class="item">
					<img src="WEB-INF/img/carousel3.jpg" alt="carousel3">
					<div class="carousel-caption">
						
					</div>
				</div>
			</div>

			/**
			 * Controls
			 */
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>-->

	<div id="signin-signup-tab" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close glyphicon glyphicon-off"
						data-dismiss="modal" aria-label="Close"></button>
					<ul class="nav nav-tabs" role="tablist">
						<li id="signin-li"><a href="#signin-tab" role="tab"
							data-toggle="tab">登陆</a></li>
						<li id="signup-li"><a href="#signup-tab" role="tab"
							data-toggle="tab">注册</a></li>
					</ul>
					<div class="tab-content" id="signin-signup-tab">
						<div class="tab-pane" id="signup-tab">
							<form class="form col-md-12 center-block">
								<div class="form-group">
									<input type="text" class="form-control input-lg" id="email"
										placeholder="请输入登陆邮件" name="email" />
								</div>
								<div class="form-group">
									<input type="text" class="form-control input-lg" id="nickname"
										placeholder="请输入用户昵称" name="phonenumber" />
								</div>
								<div class="form-group">
									<input type="password" class="form-control input-lg"
										id="password" placeholder="请输入注册密码" name="password" />
								</div>
								<div class="form-group">
									<input type="password" class="form-control input-lg"
										id="password2" placeholder="请再次输入密码" />
								</div>

								<!-- <div class="form-group">
										<input type="password" class="form-control input-lg" placeholder="请输入验证码" />
									</div> -->
								<div class="form-group">
									<input type="hidden" class="form-control input-lg"
										id="userType" placeholder="请输入用户昵称" name="userType"
										value="common" />
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control input-lg"
										id="rememberFlg" placeholder="请输入用户昵称" name="rememberFlg"
										value="1" />
								</div>
								<div class="form-group">
									<button type="button" class="btn btn-primary btn-lg btn-block"
										id="regist" />
									注册
									</button>
								</div>
							</form>
						</div>
						<div class="tab-pane" id="signin-tab">
							<form action="" class="form col-md-12 center-block">
								<div class="form-group">
									<input type="text" class="form-control input-lg"
										id="email_login" placeholder="请输入登陆邮件">
								</div>
								<div class="form-group">
									<input type="password" class="form-control input-lg"
										id="password_login" placeholder="请输入登录密码">
								</div>
								<div class="form-group">
									<button type="button" class="btn btn-primary btn-lg btn-block"
										id="login">立刻登录</button>
									<span><a href="newforgot.html">忘记密码</a></span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<script src="js/jquery-1.11.1.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/loginRegist.js"></script>
	<script>
		$('#signin-button').click(function() {
			$('#signup-li').removeClass('active');
			$('#signup-tab').removeClass('active');
			$('#signin-li').addClass('active');
			$('#signin-tab').addClass('active');
		});
		$('#signup-button').click(function() {
			$('#signin-li').removeClass('active');
			$('#signin-tab').removeClass('active');
			$('#signup-li').addClass('active');
			$('#signup-tab').addClass('active');
		});
	</script>
</body>

</html>