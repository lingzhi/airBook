/* 时间 */
define(function(require, exports) {
			require("jquery");
			require('jqDateRangePicker');
			
			$(".instatisticsReportData").each(function() {
				$(this).click(function() {
					var myDate = new Date();
					var startDate = myDate;
					var endDate = myDate;
					if ($(this).attr('id') == 'inDate1') {
						startDate = new Date(Date.parse(myDate)
								+ (86400000 * -29));
						endDate = new Date(Date.parse(myDate));
//								+ (86400000 * -1));;
						makeDate(formatDate(startDate), formatDate(endDate));
					}
					if ($(this).attr('id') == 'inDate2') {
						startDate = new Date(Date.parse(myDate)
								+ (86400000 * -6));
						endDate = new Date(Date.parse(myDate));
								//+ (86400000 * -1));;
						makeDate(formatDate(startDate), formatDate(endDate));
					}
					if ($(this).attr('id') == 'inDate3') {

						var year = myDate.getFullYear();
						var month = myDate.getMonth();
						var endDate = new Date(year, month, 1);
						endDate = new Date(Date.parse(endDate)
								+ (86400000 * -1));
						month -= 1;
						if (month == 0) {
							year -= 1;
							month = 12;
						}
						var startDate = new Date(year, month, 1);
						makeDate(formatDate(startDate), formatDate(endDate));

					}
					if ($(this).attr('id') == 'inDate4') {

						startDate = new Date(Date.parse(myDate)
								+ (86400000 * -(myDate.getDay() + 6)));
						endDate = new Date(Date.parse(myDate)
								+ (86400000 * -myDate.getDay()));
						makeDate(formatDate(startDate), formatDate(endDate));

					}
					if ($(this).attr('id') == 'inDate5') {
						startDate = new Date(Date.parse(myDate)
								+ (86400000 * -1));
						endDate = startDate;
						makeDate(formatDate(startDate), formatDate(endDate));

					}
					if ($(this).attr('id') == 'inDate6') {
						makeDate(formatDate(myDate), formatDate(myDate));

					}

				});
			});
			function makeDate(startDate, endDate) {
				$("#inStartDate").val(startDate);
				$("#inEndDate").val(endDate);
				$('#inDatePicker').data('dateRangePicker').setDateRange(startDate,endDate);
			}
			function formatDate(date) {
				var str = '';
				str += date.getFullYear() + '-';
				str += (date.getMonth() + 1) > 9 ? (date.getMonth() + 1)
						.toString() : '0' + (date.getMonth() + 1);
				str +='-';
				str += date.getDate() > 9 ? date.getDate().toString() : '0'
						+ date.getDate();
				return str;
			}
			
			$('#inDatePicker').dateRangePicker({
		    	separator : ' 至 ',
		    	language:'cn'
		    }).bind('datepicker-apply',function(event,obj){
		    	$("#inStartDate").val(obj.date1.Format("yyyy-MM-dd"));
		    	$("#inEndDate").val(obj.date2.Format("yyyy-MM-dd"));
		    	// 需要清一下错误提示
		    	$("#inDatePicker").parent().find('.error').hide();
		    });
		    
		    $('#inClearTime').click(function() {
				$("#inStartDate").val('');
				$("#inEndDate").val('');
				$('#inDatePicker').data('dateRangePicker').clear();
			})
		    
		});