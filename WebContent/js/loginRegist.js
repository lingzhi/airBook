/**
 * 次js文件应用于index.jsp的登录、注册
 */

//注册
$("#regist").on("click",function(){
	var email = $('#email').val();
	var nickname = $('#nickname').val();
	var password = $('#password').val();
	$.ajax({
		type : 'post',
		url : "/airbook/user/regist",
		data : {
			"email" : email,
			"password" : password,
			"nickname" : nickname
			},
		success : function(response) {
			if (response!=null) {
				console.log(response);
				alert("ok");
				window.open(response.data);
			}
		},function(){
			alert("失败");
		}
	});
});

//登录
$("#login").on("click",function(){
	var email = $('#email_login').val();
	var password = $('#password_login').val();
	var rememberFlg=0;
	$.ajax({
		type : 'post',
		url : "/airbook/user/login?method=email",
		data : {
			"loginName" : email,
			"password" : password,
			"rememberFlg":rememberFlg
			},
		success : function(response) {
			if (response!=null) {
				console.log(response);
				alert("ok");
				window.open(response.data);
			}
		},function(){
			alert("失败");
		}
	});
});