// JavaScript Document
define(function(require, exports) {
    require("${appPath}/osfc/core/rest.js");
    require("${appPath}/js/browser.js");
    var currentMenuName = "";
    
    // 应为遮罩层只有一个，有时候会出现多次显示遮罩的情况，这个时候，关闭遮罩时需要查看是否能关闭
    // 关闭loading的时候是否需要关闭遮罩
    var needUnmaskCloseLoadding = false;
    // 关闭确认框时是否需要关闭遮罩
    var needUnmaskCloseConfirm = false;
    // 关闭错误框时是否需要关闭遮罩
    var needUnmaskCloseError = false;

    function getMenu(){
    	if(!$(".userC_Div").html())return;
        // 获取菜单内容
        $.rest(
            'get',
            '/sec/menu',
            {},{},
            function(menus) {
                if(typeof userInfo == 'string'){
                    window.location.href="${appPath}/login.html";
                    return;
                }
                if (menus && menus.length > 0) {
                    var sideBarNav = $('#menuNav');
                    //$(".sideBar").css("overflow","auto");
                    for(var i=0; i<menus.length; i++) {
                        var menu = menus[i];
                        var navUnit = $('<div class="navUnit"><dl class="adv"></dl></div>');
                        var navDl = navUnit.find('dl');
                        if (menu.menuStyle) {
                            navDl.addClass(menu.menuStyle);
                        }

                        if (menu.children && menu.children.length > 0) {

                            if(menu.children.length==1){
                                var leafMenu = menu.children[0];
                                var menuDt = $('<dt></dt>');
                                menuDt.attr('name',leafMenu.name);
                                var menuA = $('<a ></a>');
                                menuA.html(leafMenu.menuName);
                                menuA.attr('href', '${appPath}'+leafMenu.url);
                                menuDt.append(menuA);
                                navDl.append(menuDt);

                            }else{
                                var menuDt = $('<dt></dt>');
                                menuDt.html(menu.menuName);
                                navDl.append(menuDt);
                                for (var j=0; j<menu.children.length; j++) {
                                    var menuDd = $('<dd></dd>');
                                    menuDd.attr('name',menu.children[j].name);
                                    var menuA = $('<a ></a>');
                                    menuA.html(menu.children[j].menuName);
                                    menuA.attr('href', '${appPath}'+menu.children[j].url);
                                    menuDd.append(menuA);
                                    navDl.append(menuDd);
                                }
                            }

                        }else{
                            var menuDt = $('<dt></dt>');
                            menuDt.attr('name',menu.name);
                            var menuA = $('<a ></a>');
                            menuA.html(menu.menuName);
                            menuA.attr('href', '${appPath}'+menu.url);
                            menuDt.append(menuA);
                            navDl.append(menuDt);
                        }
                        sideBarNav.append(navUnit);
                    }

                    var h=$(".sideBar").height()-$(".logo").height()-$(".placeB").height();
                    sideBarNav.css({'overflow':'hidden','overflow-y':'scroll','height':h});

                    setCurrentMenu();

                    //内容页滑过显示二级菜单
                    $('.navUnit').hover(
                        function () {
                            if (!$(this).hasClass('current')) {
                                $(this).find('dd').stop(true,true).slideDown()
                            }
                        },
                        function () {
                            if (!$(this).hasClass('current')) {
                                $(this).find('dd').stop(true,true).slideUp()
                            }
                        }
                    );
                }
            },
            function(error) {
            }
        );
    }

    function getUserName() {
    	  if(!browserObj.versions.trident && !browserObj.versions.webKit && !browserObj.versions.gecko){
          	tryOpNotice("为了更好的使用我们的网站，建议您使用如下浏览器软件的最新版本:IE,Firefox,Chrome,Safari",4000);
          }
    	if(!$(".userC_Div").html())return;
        $.rest('get', '/user/userInfo',{},{},function(userInfo) {
        	if(typeof userInfo == 'string'){
        		window.location.href="${appPath}/login.html";
        		return;
        	}
        	$('.userC').find('dt').attr("title",userInfo.userName);
        	var un=userInfo.userName;
        	if(un.length>6){
        		un=un.substring(0,6)+"...";
        	}
        	$('.userC').find('dt').html(un);
            $('.userT').val(userInfo.userType);
            $('.userC').find('dt').height(28);
            if(userInfo.userType==1){
            	tryOpNotice("试运营",3000);
            }
            setCheckResultTip(userInfo);
        });

        $('.userC_Div').find('li').click(function(){
        	_confirm("确定要退出系统吗？",function(){
        		$.rest("get", "/user/logout", {}, {}, function(){
                    window.location.href="${appPath}/login.html";
                });
        	},function(){
        		return;
        	});
            
        });
    }
    

    function resize() {
        var browser_width = $(window).width();
        var browser_height = $(window).height();
        $('.box').css('width',browser_width);
        $('.box').css('height',browser_height);
        $('.sideBar').css('height',$('.box').height());
        $('.main').css('width',$('.box').width()-$('.sideBar').width());

        $(".searchPopup").css("width",$('.main').width());
        $(".searchPopup").each(function(index, element) {
        	$(this).css("left",($('.box').width()- $(this).width())/2);
			$(this).css("top",($('.box').height() - $(this).height() - 32)/2);
        });
        //searchPopup按钮padding
        $('.searchPopup').find('.btnDiv').each(function(index, element) {
            $(this).css("padding-left",($(this).parent().width() - 180)/2);
        });

        $(".addDS_Popup").each(function(index, element) {
	        $(this).css("left",($('.box').width()- $(this).width())/2);
	        if($('.box').height() > ($(this).height() + 32)){
	        	$(this).css("top",($('.box').height() - $(this).height() - 32)/2);
	        }else{
	        	$(this).css("top", "10px");
	        }
        });

        //$(".content").css("width", $(".main").width()-61);
        //$('.content').css("height",$('.box').height() - 65);
		$('.content').css("height",$('.box').height() - 60 - 1);
		$('.openInfo').css("height",$('.box').height() - 60 - 1);

        $(".loadingBox").css("left",($('.box').width()- 240)/2);
		$(".loadingBox").css("top",($('.box').height()-80)/2);

        //强提示位置
		$(".noticeStrong").css("left",($('.box').width()- 320)/2);
		$(".noticeStrong").css("top",($('.box').height()- $(".noticeStrong").height() - 30)/2);
		
		//popup942位置
		$(".popup942").each(function(index, element) {
        	$(this).css("left",($('.box').width()- $(this).width())/2);
			$(this).css("top",($('.box').height() - $(this).height() - 32)/2);
    	});
    }

    $('#userNU').hover(
        function() {
            $('.userC_Div').show()  ;
        },
        function() {
            $('.userC_Div').hide()  ;
        }
    );
    
    var userC_Div = $('.userC_Div'),
    userC = $('.userC');
    $('#userNU').hover(
        function() {
            $('.userC_Div').show()  ;
            if(!userC.hasClass('blackbg'))
                userC.addClass('blackbg');
        },
        function() {
            $('.userC_Div').hide()  ;
            if(userC.hasClass('blackbg'))
                userC.removeClass('blackbg');
        }
    );

    $('.logo').css("cursor", "pointer");
    $('.logo').hover(function(){
    	$(this).attr('title', '首页');
    })
    $('.logo').click(function(){
        window.location.href = "${appPath}${welPageUrl}";
    });

    function hiddenCal() {
        if (typeof f_tcalCancel == 'function') {
            f_tcalCancel();
        }
    }

    $(".search").toggle(
        function(){
            if(!$(this).hasClass('currentFZ')){
                $(this).addClass('currentFZ');
                $(".searchDiv").show();
            }else{
                $(this).removeClass('currentFZ');
                $(".searchDiv").hide();
                hiddenCal();
            }
        },
        function(){
            if($(this).hasClass('currentFZ')){
                $(this).removeClass('currentFZ');
                $(".searchDiv").hide();
                hiddenCal();
            }else{
                $(this).addClass('currentFZ');
                $(".searchDiv").show();
            }
        }
    );

    $("#cancelS").click(
        function(){
            $("#search").removeClass('currentFZ');
            $(".searchDiv").hide();
            hiddenCal();
        }
    );


    function setCurrentMenu() {
        // 先清空所有navUnit div的current
        $('.navUnit').removeClass('current');
        $('.navUnit').find('dd').each(function(){
            $(this).removeClass('cur');
            if ($(this).attr('name')==currentMenuName) {
                $(this).addClass('cur');
                $(this).parent('dl').parent('div').addClass('current');
            }
        });

        $('.navUnit').find('dt').each(function(){
            $(this).removeClass('cur');
            if ($(this).attr('name')==currentMenuName) {
                $(this).addClass('cur');
                $(this).parent('dl').parent('div').addClass('current');
            }
        });

    }

//    // 设置操作弹出框显示的宽度
//    function setMoreTd() {
//        $('.moreTd').each(function(index, element) {
//            var sumWidth = 0;
//            $(this).find('li').each(function(){
//                sumWidth += $(this).width();
//            });
//            $(this).css("width",sumWidth);
//        });
//    }
    function setCheckResultTip(userInfo){
    	var url="";
    	var _utype=userInfo.userType;
    	if(_utype==1){//app
    		 url="/app/get/byloginUserId";
    	 }else if(_utype=2){//provider
    		 url="/provider/get/byloginUserId";
    	 }
    	if(url=="")return;
    	var checkResult="";
    	 $.rest('get', url,{},{},function(obj) {
    		 if(obj!=null){
    			 if(_utype==1){//app
    				 if(obj.checkStatus!="refused")return;
    				 checkResult="审核结果："+obj.checkResult+" <a href='javascript:void(0);'>点此修改</a>";
		       	 }else if(_utype=2){//provider
		       		if(obj.approveFlag!=-1)return;
		       		checkResult="您的信息提交有误，原因："+obj.approveReason+"。请修改后重新提交。如有问题可联系我们：010-82065058";
		       	 }
    			 
    			 //if(obj.checkStatus=="refused"){
    				  var hintDiv = $('.noticeWarning');
    			        if (hintDiv.length == 0) {
    			            hintDiv = $('<div class="noticeWarning" style="z-index:10;"><p></p></div>');
    			            $('body').append(hintDiv);
    			        }
    			        hintDiv.find('p').html(checkResult);
    			        hintDiv.slideDown();
    			        hintDiv.click(function(){
	    			        if(_utype==1){//app
	    			       		 window.location.href="/app/media/updateApp.html";
	    			       	 }else if(_utype=2){//provider
	    			       		 window.location.href="/provider/manager/updateProvider.html";
	    			       	 }
    			        });
    			        /*setTimeout(function(){
    			            //hintDiv.slideUp();
    			        }, 5000);*/
    			 //}
    		 }
    	 });
    }
    
   function tryOpNotice(content,timeout){
	   if(!timeout)timeout=3000;
    	var hintDiv = $('.tryopnotice');
        if (hintDiv.length == 0) {
            hintDiv = $('<div class="tryopnotice" style="z-index:9;"><p></p></div>');
            $('body').append(hintDiv);
        }
        hintDiv.find('p').html(content);
        hintDiv.slideDown();
        setTimeout(function(){
        	hintDiv.slideUp();
        }, timeout);
    }
    
   function _confirm(msg, okCallBack, cancelCallBak,zIndex){
	   var confirmDiv = $('#confirmBox');
       if (confirmDiv.length == 0) {
           confirmDiv = $('<div id="confirmBox" class="noticeStrong">'+
                          '	<p></p>'+
                          '     <div class="btnDiv">'+
                          ' 		<input type="button" class="btn100_g" value="确 认">'+
                          ' 		<span id="cancelS" class="linkG">'+
                          ' 			<a href="javascript:void(0);">取 消</a>'+
                          ' 		</span>'+
                          ' 	</div>'+
                          '</div>');
           $('body').append(confirmDiv);
       }
       if(zIndex && !isNaN(zIndex)){
       	confirmDiv.css("z-index",zIndex);
       }
       confirmDiv.find('p').html(msg);
       confirmDiv.css("left",($('.box').width()- 320)/2);
       confirmDiv.css("top",($('.box').height()- confirmDiv.height())/2);
       confirmDiv.find('a').unbind().click(function(){
           if (typeof cancelCallBak == 'function') {
               cancelCallBak();
           }
           confirmDiv.hide();
           if (needUnmaskCloseConfirm) {
               $('.maskbg').hide();
           }
       });
       confirmDiv.find('input').unbind().click(function(){
           if (typeof okCallBack == 'function') {
               okCallBack();
           }
           confirmDiv.hide();
           if (needUnmaskCloseConfirm) {
               $('.maskbg').hide();
           }
       });

       confirmDiv.show();
       
       if ($('.maskbg').css("display") != 'block') {
           $('.maskbg').show();
           needUnmaskCloseConfirm = true;        
       } else {
           needUnmaskCloseConfirm = false;
       }
   }
   
    exports.resize = function(){
        resize();
    }

    exports.setCurrentMenu = function(name) {
        currentMenuName = name;
    }

    exports.hiddenSearch = function() {
        $('.search').removeClass('currentFZ');
        $('.searchDiv').hide();
        hiddenCal();
    }

    exports.initOperation = function() {
        $('.setIcon').hover(
            function(){
                $(this).parent().find('.moreTd').show();
            }
        );
        setMoreTd();
        $('.moreTd').mouseleave(function(){
            $(this).hide();
        });
    }

    
    exports.confirm = function(msg, okCallBack, cancelCallBak,zIndex) {
    	_confirm(msg, okCallBack, cancelCallBak,zIndex);
    }
    
    exports.error = function(msg, callback) {
    	var errorBox = $('#errorBox');
        if (errorBox.length == 0) {
        	errorBox = $('<div id="errorBox" class="noticeStrong">'+
                           '	<p class="wrong"></p>'+
                           '     <div class="btnDiv">'+
                           ' 		<input type="button" class="btn100_g" value="确 认">'+
                           ' 	</div>'+
                           '</div>');
            $('body').append(errorBox);
        }
        errorBox.find('p').html(msg);
        errorBox.css("left",($('.box').width()- 320)/2);
        errorBox.css("top",($('.box').height()- errorBox.height())/2);
        errorBox.find('input').unbind().click(function(){
            if (typeof callback == 'function') {
            	callback();
            }
            errorBox.hide();
            if (needUnmaskCloseError) {
                $('.maskbg').hide();
            }
        });

        errorBox.show();
        
        if ($('.maskbg').css("display") != 'block') {
            $('.maskbg').show();
            needUnmaskCloseError = true;        
        } else {
        	needUnmaskCloseError = false;
        }
    }

    exports.hint = function(msg,timeout){
        if(!timeout)timeout=3000;
    	var hintDiv = $('.noticeWeak');
        if (hintDiv.length == 0) {
            hintDiv = $('<div class="noticeWeak" style="z-index:10;"><p></p></div>');
            $('body').append(hintDiv);
        }
        hintDiv.find('p').html(msg);
        hintDiv.slideDown();
        setTimeout(function(){
            hintDiv.slideUp();
        }, timeout);
    }

    exports.popUp = function(divObj, okCallBack, cancelCallBak) {
        divObj.show();
        $('.maskbg').show();
        divObj.find('.btnDiv').find('input[type="button"]').unbind().click(function(){
            if (typeof okCallBack == 'function') {
                var ret = okCallBack();
                if (ret == undefined || ret) {
                    divObj.hide();
                    $('.maskbg').hide();
                }
            }
        });
        divObj.find('.btnDiv').find('a').unbind().click(function(){
            if (typeof cancelCallBak == 'function') {
                var ret = cancelCallBak();
                if (ret == undefined || ret) {
                    divObj.hide();
                    $('.maskbg').hide();
                }
            }
        });
    }
    
    exports.showLoading = function() {
        $('.loadingBox').css('z-index', 14);
    	$('.loadingBox').show();
    	if ($('.maskbg').css("display") != 'block') {
            $('.maskbg').show();
            needUnmaskCloseLoadding = true;        
        } else {
            // 提升maskbg的z-index
            $('.maskbg').css('z-index', 13);
            needUnmaskCloseLoadding = false;
        }
    }
    exports.confirmss = function(msg, okCallBack, cancelCallBak) {
        var confirmDiv = $('#confirmBox');
        if (confirmDiv.length == 0) {
            confirmDiv = $('<div id="confirmBox" class="noticeStrong">'+
                           '	<p></p>'+
                           '     <div class="btnDiv">'+
                           ' 		<input type="button" class="btn100_g" value="确 认">'+
                           ' 		<span id="cancelS" class="linkG">'+
                           ' 			<a href="javascript:void(0);">取 消</a>'+
                           ' 		</span>'+
                           ' 	</div>'+
                           '</div>');
            $('body').append(confirmDiv);
        }
        confirmDiv.find('p').html(msg);
        confirmDiv.css("left",($('.box').width()- 320)/2);
        confirmDiv.css("top",($('.box').height()- confirmDiv.height())/2);
        confirmDiv.find('a').unbind().click(function(){
            if (typeof cancelCallBak == 'function') {
                cancelCallBak();
            }
            confirmDiv.hide();
            if (needUnmaskCloseConfirm) {
                $('.maskbg').hide();
            }
        });
        confirmDiv.find('input').unbind().click(function(){
            if (typeof okCallBack == 'function') {
                okCallBack();
            }
            confirmDiv.hide();
            if (needUnmaskCloseConfirm) {
                $('.maskbg').hide();
            }
        });

        confirmDiv.show();
        
        if ($('.maskbg').css("display") != 'block') {
            $('.maskbg').show();
            needUnmaskCloseConfirm = true;        
        } else {
            needUnmaskCloseConfirm = false;
        }
        
    }
    exports.hideLoading = function() {
        $('.loadingBox').hide();
        if (needUnmaskCloseLoadding) {
            $('.maskbg').hide();
        } else {
            //  恢复maskbg的z-index
            $('.maskbg').css('z-index', 9);
        }
    }

    resize();
    getMenu();
    getUserName();
    
    $(window).resize(function() {
        resize();
    });
});
